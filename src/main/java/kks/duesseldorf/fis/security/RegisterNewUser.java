package kks.duesseldorf.fis.security;

import kks.duesseldorf.fis.service.model.User;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Service
public class RegisterNewUser {
    
    @RequestMapping("/registerNewUser")
    public ModelAndView register(
    @RequestParam String username, String password, String checkpassword, String email) {
        ModelAndView mav = new ModelAndView();
        
        mav.addObject("page", "login.jsp");
        mav.setViewName("header");
        return mav;
    }
    
    private User assembleNewUser(String username, String password, String email) {
        return new User(username, password, email, Boolean.FALSE);
    }
    
}
