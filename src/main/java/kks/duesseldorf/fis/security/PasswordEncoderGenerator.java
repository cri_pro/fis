package kks.duesseldorf.fis.security;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


public class PasswordEncoderGenerator {
    BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
    
    public String encode(String password) {
        return bCryptPasswordEncoder.encode(password);
    }
   
}