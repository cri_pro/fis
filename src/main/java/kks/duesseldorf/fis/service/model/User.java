package kks.duesseldorf.fis.service.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="users")
public class User implements Serializable {
    
    @Id
    @Column(unique = true, nullable = false, length = 45)
    @Getter @Setter private String username;
    
    @Column(nullable = false, length = 60)
    @Getter @Setter private String password;
    
    @Column(nullable = false, length = 100)
    @Getter @Setter private String email;
    
    @Column(nullable = false)
    @Getter @Setter private Boolean enabled;
    
    public Boolean isEnabled() {
        return this.enabled;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    @Getter @Setter private Set<UserRole> userRole;

    public User(String username, String password, String email, Boolean enabled) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.enabled = enabled;
    }
    
}