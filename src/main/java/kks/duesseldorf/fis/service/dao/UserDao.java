package kks.duesseldorf.fis.service.dao;

import java.util.Set;
import kks.duesseldorf.fis.service.model.User;
import kks.duesseldorf.fis.service.model.UserRole;

public interface UserDao {
    
    User findByUsername(String username);
    Boolean checkByUsername(String username);
    void saveNewUser(User user);
    void setNewUserRoles(Set<UserRole> userRoles);
    
}