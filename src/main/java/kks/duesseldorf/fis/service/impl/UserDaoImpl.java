package kks.duesseldorf.fis.service.impl;

import java.util.Set;
import javax.transaction.Transactional;
import kks.duesseldorf.fis.service.dao.UserDao;
import kks.duesseldorf.fis.service.model.User;
import kks.duesseldorf.fis.service.model.UserRole;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl implements UserDao {
    
    @Autowired
    @Getter @Setter SessionFactory sessionFactory;
    
    @Override @SuppressWarnings("unchecked") @Transactional
    public User findByUsername(String username) {
        return (User) sessionFactory.getCurrentSession()
                .createCriteria(User.class)
                .add(Restrictions.eq("username", username))
                .uniqueResult();
    }

    @Override @Transactional
    public Boolean checkByUsername(String username) {
        return sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("username", username)).list().size() > 0;
    }

    @Override @Transactional
    public void saveNewUser(User user) {
        sessionFactory.getCurrentSession().save(user);
    }

    @Override @Transactional
    public void setNewUserRoles(Set<UserRole> userRoles) {
        for (UserRole userRole : userRoles) {
            sessionFactory.getCurrentSession().save(userRole);
        }
    }
    
}
